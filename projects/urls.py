from django.urls import path

from projects.views import (
    ProjectsListView,
    ProjectsDetailView,
    ProjectCreateView,
)


urlpatterns = [
    path("", ProjectsListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectsDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
